<img src="images/IDSNlogo.png" width="200" height="200"/>

# Project Scenario and Tasks


In the previous modules, you learned how to train a custom Visual Recognition classifier. For the project in this module, you will develop a new custom classifier and learn how to deploy it as a web app on the cloud.

There are various advantages to deploying your Visual Recognition classifier to the cloud. First, you can access you can share your classifier and have anyone in the world, access it by entering the URL of your model into any web browser. Second, you can display your web app on your portfolio, and your potential future employers can interact with your project.


# PROJECT SCENARIO:

The scenario: You’re working as an Artificial Intelligence Specialist for a disaster response and rescue service. The service is developing a program that will allow people to submit drone images of their property or town after a storm that can help the service determine how to focus relief efforts in the event of a major storm. They want quickly to evaluate whether the images are usable in their evaluation by categorizing the images they receive into those that show storm damages and those that don't. The student will use Watson Visual Recognition and OpenCV to develop a visual recognition model that will be able to detect storm damage in the photos they get from submitters helping them better manage submissions for their relief program.

You will start by developing a Visual Recognition model that will classify any image as showing hurricane damage or showing no damage. You will utilize Watson Studio and the Watson Visual Recognition Service to develop this model and then deploy it as a web app by completing the tasks below.

**Task 1: Gathering the Dataset**

**Task 2: Training your Classifier**

**Task 3: Deploying your Web App**

**Task 4: Testing your Classifier**

**Task 5: Submit your Assignment and Evaluate your Peers** (for non-audit i.e. paid learners only)

Each of the above tasks will be described in detail in the sections that follow.