<img src="images/IDSNlogo.png" width="200" height="200"/>

# Submit Your Project

General Instructions:

After completing the required Tasks, you need to submit your work for peer review.

To get a good grade, please :

1. Check your work before you submit it

2. Upload your class score as shown figure

<img  src="images/Task 5: Submit your Assignment and Review Peers/task35_2.png" />

3. Share your classifier web app URL at the end of this section

<img  src="images/Task 5: Submit your Assignment and Review Peers/task5_2.png" />

**General Grading Instructions**:

**Not provided**: The class score for the fire provided image is either not provided or the class score is provided but not right ( as an example, giving a damage label to a no damage image). 

**Poor**: The class score for a specific provided image is less than 0.4

**Fair**: The class score for a specific provided image is more than 0.4 but less than 0.6 

**Good**: The class score for a specific provided image is more than 0.6 but less than 0.8

**Excellent**: The class score for a specific provided image is more than 0.8



## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2020-10-09 | 2.0 | Arpita | Migrated Lab to Markdown and added to course repo in GitLab |


## <h3 align="center"> © IBM Corporation 2020. All rights reserved. <h3/>

