<img src="images/IDSNlogo.png" width="200" height="200"/>

# Task 2: Training your Classifier

### Task 2: TRAINING YOUR CLASSIFIER

Congratulations, you are done with the toughest part, which is gathering the images dataset.  Now is the FUN part in which you will use Watson Visual Recognition to train your Classifier. In Module 4, you learned how to create a custom classifier using Watson Visual Recognition.

To create and train the classifier, you will use the Jupyter notebook that comes with this exercise called **Training_Custom_Classifiers_with_Watson_skillup.online.ipynb**. Use Watson Studio to create a new notebook resource and deploy the notebook to the resource. Follow the instructions in the notebook using the images you just collected to create and train the classifier. Once you’ve completed the notebook, you’ll be ready for the next task in this exercise. 

To use the notebook in Watson Studio, follow these steps.

#### _Step 1: Start Watson Studio_

From your Dashboard, choose the Watson Studio resource and click the Get Started button to open Watson Studio.

<img src="images/Task 2: Training your Classifier/2-Watston-studio-get-started.png"/>


For reference, here is the link to Watson Visual Recognition service on IBM Cloud: [https://cocl.us/CV0101EN_IBM_Cloud_Registration](https://cocl.us/CV0101EN_IBM_Cloud_Registration)


#### _Step 2: Open or Create a Project_

If you already have a project in Watson Studio, you can open it and add the notebook to it or you can choose to create a new project. First, open the Projects section of Watson Studio to see a list of the projects you currently have.

<img src="images/Task 2: Training your Classifier/3-Watson-studio-open-projects.png"/>


Once you’re in the Projects section, click on an existing project or click the **New Project** button to create a new project. 

<img src="images/Task 2: Training your Classifier/4-watson-studio-new-or-open-proj.png"/>

#### _Step 3: Add the Notebook to the Project_

Once you’re in your project, find the **Notebooks** section and click the **New Notebook** button. 

<img src="images/Task 2: Training your Classifier/5-watson-new-notebook.png"/>

To add the notebook, choose the **From File** tab on the next screen, drag the **Training_Custom_Classifiers_with_Watson_skillup.online.ipynb** to the target area on the left side and click the **Create** button.


<img src="images/Task 2: Training your Classifier/6-watson-create-new-notebook.png"/>

Once the notebook loads into Watson Studio, you need to put it into edit mode and trust it.

#### _Step 4: Edit and Trust the Notebook_

To put the notebook into edit mode, click the pencil icon in the toolbar.

<img src="images/Task 2: Training your Classifier/7-notebook-edit-mode.png"/>

Finally, you’ll want to make sure your notebook is trusted. When you first load it, it won’t be trusted so click the **Not Trusted** button to trust the notebook to run code.

<img src="images/Task 2: Training your Classifier/8-notebook-trust.png"/>

Now you’re ready to follow the instructions in the notebook to create and train your classifier!




## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2020-10-09 | 2.0 | Arpita | Migrated Lab to Markdown and added to course repo in GitLab |


## <h3 align="center"> © IBM Corporation 2020. All rights reserved. <h3/>
