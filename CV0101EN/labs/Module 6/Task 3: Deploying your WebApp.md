<img src="images/IDSNlogo.png" width="200" height="200"/>

# Task 3: Deploying your Web App

**Task 3: Deploying Your Web App**

The instructions for deploying your model as a Web App on the cloud are found in the Jupyter notebook in next section. Load the Jupyter notebook called **Capstone-Project-Model-Deployment-edx.ipynb** included with this course using the same method from the previous task and follow the step by step instructions and run the notebook with your Watson Visual Recognition credentials (email and password) to deploy your Web App on the cloud.

After deploying your Web App to the cloud, you will be able to access your Visual Recognition classifier as a webpage, that will look similar to the image below.


<img src="images/Task 3: Deploying your Webapp/task3_1.png"/>

After Uploading your image in uploading area of the Web app, you will expect to get similar to the below result:

<img src="images/Task 3: Deploying your Webapp/task3_2.png"/>




## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2020-10-09 | 2.0 | Arpita | Migrated Lab to Markdown and added to course repo in GitLab |


## <h3 align="center"> © IBM Corporation 2020. All rights reserved. <h3/>