<img src="images/IDSNlogo.png" width="200" height="200"/>

# Task 4: Testing Your Classifier

After you deploy your Visual Recognition classifier, anyone can classify images based on damage or no damage using your classifier using the URL of your web app. All they have to do is enter the URL into any browser and upload an image. And they will get a classification of the image along with its class score.

In Task-1, you gathered 160 images for each label (damage and no damage) out of which 5 % (approximately 10 images for each label) you kept for testing. You can use these testing images to test your classifier as shown below, before submitting your URL web app for grading.


<img src="images/Task 4: Testing your Classifier/task4_1.png">





## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2020-10-09 | 2.0 | Arpita | Migrated Lab to Markdown and added to course repo in GitLab |


## <h3 align="center"> © IBM Corporation 2020. All rights reserved. <h3/>
